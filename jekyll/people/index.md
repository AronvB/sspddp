---
layout: default
header: People
permalink: /people/
---
_Investigators_:

* [Marc Stevens](https://www.cwi.nl/people/marc-stevens) (Centrum Wiskunde & Informatica)
* [Tijs van der Storm](https://homepages.cwi.nl/~storm/) (Centrum Wiskunde & Informatica)
* [Henri Bal](https://www.vuhpdc.net/henri-bal/) (Vrije Universiteit Amsterdam)
* Tom van Engers (University of Amsterdam)
* [Cees de Laat](https://delaat.net/) (University of Amsterdam)
* Sander Klous (University of Amsterdam)
* Yurry Hendriks (ABN AMRO)
* Peter van den Hoven (ING)
* Leon Gommans (AirFrance-KLM)

_Funded Researchers_:

* [L. Thomas van Binsbergen](http://ltvanbinsbergen.nl) (Centrum Wiskunde & Informatica)
* [Marc X. Makkes](https://www.cs.vu.nl/~mxm/) (Vrije Universiteit Amsterdam)

_Funded PhD students_:

* Lu-Chi Liu (University of Amsterdam)
* Aron van Baarsen (Centrum Wiskunde & Informatica)

_Affiliated researchers_:

* Giovanni Sileno (University of Amsterdam)

_Affiliated PhD students_:

* Esteban Landerreche (Centrum Wiskunde & Informatica)

_Industry partners_:

* Ryan Bulthuis (ABN AMRO)
* Vincent van Vliet (ABN AMRO)
* Scott King (ING)
* Tommy Koens (ING)
