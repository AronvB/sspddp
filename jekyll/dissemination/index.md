---
layout: default
header: Dissemination Event
permalink: /dissemination/
---
#### Abstract
The SSPDDP project is a collaboration between the knowledge institutes UvA, CWI and VU and the industrial partners ABN AMRO, ING and KLM to develop secure and scalable techniques for policy-enforced data sharing. The project investigates fundamental research questions to develop future-proof, cryptographic solutions to secure and trustworthy data sharing, automatic policy enforcement in distributed systems and bringing the aforementioned data sharing methods to scale. Besides being academically relevant, the outcomes of these investigations suggest a principled, sound and pragmatic mode of operation that can add value to the industry partners in several ways. The purpose of this event is for the academic partners to share their initial research results and to discuss ways in which the proposed methods for data sharing can be applied by the industrial partners. 

The Know Your Customer (KYC) guidelines for financial institutions are discussed as an illustrative example of the wider approach. An institution's assessment of a client's risk can be improved by considering also the data collected by industrial partners and competitors. A sharing agreement between financial institutions can facilitate sharing of client data without revealing sensitive data or losing competitive advantages. However, to participate in such sharing activities, a high degree of trust in the implementing architecture is necessary, both in terms of security of the system as well as of the system's compliance with the sharing agreement. Moreover, data handling needs to adhere to internal policies, government regulations (such as GDPR and WWTF) and branch-wide codes.

As part of the SSPDDP project, techniques are developed that enable data sharing for improved KYC risk assessment. Cryptographic solutions guarantee future-proof security and identity management in data sharing. New distributed ledger technologies bring ledgers to clients with restricted resources. Regulatory services enforce compliance with contracts (such as sharing agreements), regulations, policies and branch-wide codes. The latest research outcomes of the project will be presented, inviting a discussion on the requirements of industry partners to adopting these technologies.

#### Program

1 September 2020, Virtual session  
14:00 _Opening_ by Marc Stevens and Ryan Bulthuis [[Slides]]({{ "/files/slides/dissemination_intro.pdf" | prepend:site.baseurl }})  
14:25 _WP3: Policy-enhanced data-exchange -- goals, results and plans_ by [L. Thomas van Binsbergen](http://ltvanbinsbergen.nl) and Lu-Chi Liu [[Slides]]({{ "/files/slides/dissemination_wp3.pdf"  | prepend:site.baseurl }})   
15:15 _WP1: Security, Trust and Privacy_ by [Marc Stevens](https://www.cwi.nl/people/marc-stevens) [[Slides]]({{ "/files/slides/dissemination_wp1.pdf"  | prepend:site.baseurl }})    
15:55 break   
16:05 _WP2: Scalability and distributed Bigdata_ by [Marc X. Makkes](https://www.cs.vu.nl/~mxm/) [[Slides]]({{ "/files/slides/dissemination_wp2.pdf" | prepend:site.baseurl}})  
16:30 end

#### Feedback

All participants are kindly requested to provide any form of feedback -- suggestions, comments, questions, etc.   
The feedback is collected in a [Dropbox Paper](https://paper.dropbox.com/doc/Dissemination-event-feedback-collection--A63Vb0yZTaSNrtp4w6OxUQmkAQ-DL6RMylLZJkCBYmEk1L4n) document.   
In writing the feedback, please consider the following questions:

* did you gain any specific new insights into the topics discussed?
* are there any topics about which you would have liked to hear more?
* can you imagine your organization might benefit from the ongoing and planned research directions that have been presented?
  * if so, in what way might this happen? do you see any specific hurdles to overcome?
  * if not, what are the main issues that need to be addressed?
